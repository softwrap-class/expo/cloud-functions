const admin = require('firebase-admin');
admin.initializeApp();

exports.collectionTriggers = require('./collectionTriggers')
exports.api = require('./api');
