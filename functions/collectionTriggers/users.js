const functions = require('firebase-functions');
const admin = require('firebase-admin');

exports.initializeUserData = functions.auth.user().onCreate(async (user) => {
  try {
    await admin.firestore()
      .collection('users')
      .doc(user.uid)
      .set({
        createdAt: new Date(),
        email: user.email,
      })
  } catch (e) {
    throw new functions.https.HttpsError('internal', e.message);
  }
});
