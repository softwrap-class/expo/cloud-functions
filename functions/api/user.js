const functions = require('firebase-functions');
const admin = require('firebase-admin');

exports.updateUserData = functions.https.onCall(async (data, context) => {
  if (!data || Object.keys(data).length <= 0) {
    throw new functions.https.HttpsError('invalid-argument', 'No Data');
  }

  const { uid } = context.auth;

  if (!uid) throw new functions.https.HttpsError('permission-denied', 'No UID');

  const user = await admin.firestore().collection('users').doc(uid).get();
  if (!user.exists) throw new functions.https.HttpsError('not-found', 'User not found');

  try {
    await admin.firestore().collection('users').doc(uid).update({
      ...data,
      modifiedAt: new Date(),
      modifiedBy: uid,
    });
  } catch (e) {
    throw new functions.https.HttpsError('internal', e.message);
  }
})
